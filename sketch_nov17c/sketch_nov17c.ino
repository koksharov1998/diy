#include <LedControl.h>
const int dataPin = 12;
const int csPin = 11;
const int clockPin = 10;
const int echoPin = 8;
const int trigPin = 9;
const int sensorPin = A0;
const float voltsPerMeasurment = 5.0 / 1024;

LedControl disp = LedControl(dataPin, clockPin, csPin, 1);

void setup() {
  Serial.begin(115200);
  disp.shutdown(0, false);
  disp.setIntensity(0, 16);
  disp.clearDisplay(0);

  pinMode(sensorPin, INPUT);


  pinMode(echoPin, INPUT);
  pinMode(trigPin, OUTPUT);

  digitalWrite(trigPin, LOW);
}

void loop() {
  int a = (int)readDistUltrasonic();
  int distX = constrain(a, 20, 100)-20;
  int b = (int)readDist(sensorPin);
  int distY = constrain(b, 20, 100)-20;
  disp.setLed(0, distX/10, distY/10, true);
  delay(500);
  //disp.setLed(0, distX/10, distY/10, false);
  
}
float readDist(int pin) {
  float volts = readFlexAnalog(pin)* voltsPerMeasurment;
  return pow(14.7737/volts, 1.2134);
}
float readDistUltrasonic() {
  long pulse = readPulse();
  return pulse * (340.0 * 100.0) / (1000.0 * 1000.0 * 2.0);
}

long readPulse() {
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  return pulseIn(echoPin, HIGH);
}
float readFlexAnalog(int pin) {
  int sum = 0;
  int maxV = -1;
  int minV = 1024;

  int n = 12;

  for (int i =0; i<n;i++)
  {
    int current = analogRead(pin);
    if (maxV < current) {
      maxV = current;
    }
    if (minV > current) {
      minV = current;
    }
    sum = sum + current;
    delay(5);
  }
  return (sum - maxV - minV) / (float)(n-2);
}
