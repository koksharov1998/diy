const int sensorPin = A0;
const float voltsPerMeasurment = 5.0 / 1024;

void setup() {
  Serial.begin(115200);
  pinMode(sensorPin, INPUT);
}

void loop() {
  int value = readDist(sensorPin);
  Serial.println(value);
}

float readDist(int pin) {
  float volts = readFlexAnalog(pin)* voltsPerMeasurment;
  return pow(14.7737/volts, 1.2134);
}

float readFlexAnalog(int pin) {
  int sum = 0;
  int maxV = -1;
  int minV = 1024;

  int n = 12;

  for (int i =0; i<n;i++)
  {
    int current = analogRead(pin);
    if (maxV < current) {
      maxV = current;
    }
    if (minV > current) {
      minV = current;
    }
    sum = sum + current;
    delay(5);
  }
  return (sum - maxV - minV) / (float)(n-2);
}
