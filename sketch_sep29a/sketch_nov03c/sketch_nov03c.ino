#include <Servo.h>
Servo baseServo;
Servo laserServo;
void setup() {
  baseServo.attach(9); // Используем пин номер 9.
  laserServo.attach(10);
}
void loop() {
  for (int i = 0; i<=180; i++)
  {
  baseServo.write(i);
  laserServo.write(i);
  delay(15);
  }
  for (int i = 180; i>=0; i--)
  {
  baseServo.write(i);
  laserServo.write(i);
  delay(15);
  }
}
