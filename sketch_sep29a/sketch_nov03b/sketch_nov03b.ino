void setup() {
  pinMode(9, OUTPUT); // Выбираем пин номер 9 для передачи команд
}

void loop() {
  analogWrite(9, 0); // подаём напряжение на пин номер 9 0V
  delay(1000); // Ждём
  
  analogWrite(9, 50);
  delay(1000);
  
  analogWrite(9, 100);
  delay(1000);
  
  analogWrite(9, 150);
  delay(1000);

  analogWrite(9, 200);
  delay(1000);

  analogWrite(9, 250);
  delay(1000);
}
