void setup() {
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
}

void loop() {
  setRGB(255, 0, 0);
  delay(1000);
  setRGB(0, 255, 0);
  delay(1000);
  setRGB(0, 0, 255);
  delay(1000);
}

void setRGB(int red, int green, int blue) {
  analogWrite(6, 255 - red);
  analogWrite(7, 255 - green);
  analogWrite(8, 255 - blue);
}
