#include <Arduino.h>
#include <MD_TCS230.h>
#include <Servo.h>
Servo servo1;
Servo servo2;

#define  S0_OUT  5
#define  S1_OUT  6
#define  S2_OUT  7
#define  S3_OUT  8
MD_TCS230 *ColorSensor;

#define R_OUT 9
#define G_OUT 10
#define B_OUT 11

const int servo1Pin = 2;
const int servo2Pin = 3;

void initSensor(MD_TCS230 *sensor) {
    sensorData whiteCalibration;
    whiteCalibration.value[TCS230_RGB_R] = 83150;
    whiteCalibration.value[TCS230_RGB_G] = 79390;
    whiteCalibration.value[TCS230_RGB_B] = 110810;

    sensorData blackCalibration;
    blackCalibration.value[TCS230_RGB_R] = 11260;
    blackCalibration.value[TCS230_RGB_G] = 9960;
    blackCalibration.value[TCS230_RGB_B] = 13690;

    sensor->setDarkCal(&blackCalibration);
    sensor->setWhiteCal(&whiteCalibration);
}
  // 75 - край
void setup()
{

  Serial.begin(115200);
  Serial.println("Started!");

  servo1UpdatePosition(70, 900);
  servo2UpdatePosition(10, 900);

  pinMode(R_OUT, OUTPUT);
  pinMode(G_OUT, OUTPUT);
  pinMode(B_OUT, OUTPUT);
}

void loop() 
{
    ColorSensor = new MD_TCS230(S2_OUT, S3_OUT, S0_OUT, S1_OUT);
    ColorSensor->begin();
    initSensor(ColorSensor);
    
    // Reading data from color sensor
    colorData rgb;
    ColorSensor->read();
    while (!ColorSensor->available());
    ColorSensor->getRGB(&rgb);
    delete ColorSensor;
    print_rgb(rgb);
    
    int col = get_colour(rgb);
//    Seria/l.print(col);

    if (col == 0) {
      return;
    }
    
    servo2UpdatePosition((col - 1) * 22 + 10, 500);
    servo1UpdatePosition(0, 500);
    servo1UpdatePosition(70, 500);
    delay(700);
  
}

void servo1UpdatePosition(int newPosition, int delayMs) {
  servo1.attach(servo1Pin);
  servo1.write(newPosition);
  delay(delayMs);
  servo1.detach();
}

void servo2UpdatePosition(int newPosition, int delayMs) {
  servo2.attach(servo2Pin);
  servo2.write(newPosition);
  delay(delayMs);
  servo2.detach();
}

void print_rgb(colorData rgb)
{
  Serial.print(rgb.value[TCS230_RGB_R]);
  Serial.print(" ");
  Serial.print(rgb.value[TCS230_RGB_G]);
  Serial.print(" ");
  Serial.print(rgb.value[TCS230_RGB_B]);
  Serial.println();
}

int get_colour(colorData rgb)
{
  int cred = get_diff(12, 2, 3, rgb);
  int cyellow = get_diff(12, 10, 4, rgb);
  int cblue = get_diff(0, 5, 12, rgb);
  int cgreen = get_diff(5, 10, 4, rgb);
  int corange = get_diff(15, 6, 4, rgb);
  
  int cnone = get_diff(0,0,0,rgb);
  int our_min = min(min(min(min(min(cred, cgreen),cblue),corange),cyellow),cnone);
  if (our_min == cnone)
    return 0; // none
  if (our_min == cred)
    return 1; //red
  if (our_min == cgreen)
    return 2; //green
  if (our_min == cblue)
    return 3; //blue
  if (our_min == corange)
    return 4; //orange
  if (our_min == cyellow)
    return 5; //yellow
}

int get_diff(int red, int green, int blue, colorData rgb2)
{
  return pow(red - rgb2.value[TCS230_RGB_R],2)+ pow(green - rgb2.value[TCS230_RGB_G], 2)+ pow(blue - rgb2.value[TCS230_RGB_B],2);
}
  
