#include <Servo.h>
Servo baseServo;
Servo laserServo;
void setup() {
  baseServo.attach(9);
  laserServo.attach(10);
  Serial.begin(115200);
  updatePosition(0, 0, 1000);
}
 
void loop() {
  char command = Serial.read();
  if(command == 'M'){
    move();
  }
  if(command == 'S'){
    scenario();
  }
}
void move(){
  int basePosition = Serial.parseInt();
  int laserPosition = Serial.parseInt();
  updatePosition(basePosition, laserPosition, 0);
}
 
void scenario(){
  updatePosition(90, 90, 500);
  updatePosition(110, 110, 0);
}
 
void updatePosition(int basePosition, int laserPosition, int delayMs){
  baseServo.write(basePosition);
  laserServo.write(laserPosition);
  delay(delayMs);
  Serial.print("current position: ");
  Serial.print(basePosition);
  Serial.print(", ");
  Serial.println(laserPosition);
}
