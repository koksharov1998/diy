const int echoPin = 8;
const int trigPin = 9;

void setup() {
  Serial.begin(115200);
  pinMode(echoPin, INPUT);
  pinMode(trigPin, OUTPUT);

  digitalWrite(trigPin, LOW);
}

void loop() {
  float dist = readDistUltrasonic();
  Serial.println(dist);
  delay(20);
}

float readDistUltrasonic() {
  long pulse = readPulse();
  return pulse * (340.0 * 100.0) / (1000.0 * 1000.0 * 2.0);
}

long readPulse() {
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  return pulseIn(echoPin, HIGH);
}
